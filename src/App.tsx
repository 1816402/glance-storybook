import React from 'react';
import 'glance-component-library/dist/styles.css';
import { TextButton } from 'glance-component-library';
import { NavBar } from './components/Navbar/NavBar';
import './App.css';

function App() {
  return (
    <div className="App">
      <TextButton buttonText={'photography & video production'} linkLocation={'/'} />
      <NavBar navCategories={['design','photography & video production']} />
    </div>
  );
}

export default App;
