import React, { Component } from 'react'
import { TextButton } from 'glance-component-library';
import './navBar.scss';

interface NavBarProps {
  navCategories: Array<string>;
}

export class NavBar extends Component <NavBarProps> {
  render() {
    const { navCategories } = this.props;
    return (
      <div className='nav-menu'>
          {navCategories.map((value,index) => {
            return (
              <li key={index}>
                <TextButton buttonText={value} linkLocation={'/'} />
              </li>
            )
          })}
      </div>
    )
  }
}

export default NavBar
