import React from 'react';
import { text, array } from '@storybook/addon-knobs';

import { TextButton } from 'glance-component-library';
import { NavBar } from '../components/Navbar/NavBar';
import 'glance-component-library/dist/styles.css';

export default {
  title: 'Button'
};

export const textButton = () => (
  <TextButton
    buttonText={text("button text", "Design")}
    linkLocation={text("link location", "/")}
  />
);

export const navBar = () => {
  return <NavBar navCategories={array('nav categories(array)',['design','photography & video production','other'],', ')} />
}

navBar.story ={
  parameters: {
    notes: '& only appears &amp; in storybook'
  }
}