import 'font-awesome/css/font-awesome.min.css';
import { addDecorator } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';

addDecorator(withKnobs({escapeHTML:false}))