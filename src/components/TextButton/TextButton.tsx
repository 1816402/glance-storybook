import React, { Component } from 'react';
import  './textButton.scss';

type textButtonProps = {
    buttonText:string;
    linkLocation:string;
}

export class TextButton extends Component <textButtonProps> {
    render() {
        const { buttonText, linkLocation } = this.props;
        return (
            <a className="text-button" href={linkLocation}>
                {buttonText}
            </a>
        )
    }
}

export default TextButton
